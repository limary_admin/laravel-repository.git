<?php
namespace Sinta\LRepository\Traits;

trait TransformableTrait
{
    public function transform()
    {
        return $this->toArray();
    }
}