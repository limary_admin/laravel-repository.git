<?php
namespace Sinta\LRepository\Traits;

use Illuminate\Support\Arr;

use Sinta\LRepository\Contracts\PresenterInterface;

trait PresentableTrait
{
    protected $presenter = null;


    public function setPresenter(PresenterInterface $presenter)
    {
        $this->presenter = $presenter;
        return $this;
    }

    public function present($key, $default = null)
    {
        if ($this->hasPresenter()) {
            $data = $this->presenter()['data'];
            return Arr::get($data, $key, $default);
        }
        return $default;
    }


    protected function hasPresenter()
    {
        return isset($this->presenter) && $this->presenter instanceof PresenterInterface;
    }

    /**
     * @return $this|mixed
     */
    public function presenter()
    {
        if ($this->hasPresenter()) {
            return $this->presenter->present($this);
        }
        return $this;
    }
}