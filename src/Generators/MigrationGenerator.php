<?php
namespace Sinta\LRepository\Generators;

use Sinta\LRepository\Generators\Migrations\NameParser;
use Sinta\LRepository\Generators\Migrations\SchemaParser;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class MigrationGenerator extends Generator
{

    protected $stub = 'migration/plain';

    public function getBasePath()
    {
        return base_path() . '/database/migrations/';
    }

    public function getPath()
    {
        return $this->getBasePath() . $this->getFileName() . '.php';
    }

    public function getPathConfigNode()
    {
        return '';
    }

    public function getRootNamespace()
    {
        return '';
    }

    public function getMigrationName()
    {
        return strtolower($this->name);
    }

    public function getFileName()
    {
        return date('Y_m_d_His_') . $this->getMigrationName();
    }

    public function getSchemaParser()
    {
        return new SchemaParser($this->fields);
    }

    public function getNameParser()
    {
        return new NameParser($this->name);
    }

    public function getStub()
    {
        $parser = $this->getNameParser();

        $action = $parser->getAction();
        switch ($action) {
            case 'add':
            case 'append':
            case 'update':
            case 'insert':
                $file = 'change';
                $replacements = [
                    'class'       => $this->getClass(),
                    'table'       => $parser->getTable(),
                    'fields_up'   => $this->getSchemaParser()->up(),
                    'fields_down' => $this->getSchemaParser()->down(),
                ];
                break;

            case 'delete':
            case 'remove':
            case 'alter':
                $file = 'change';
                $replacements = [
                    'class'       => $this->getClass(),
                    'table'       => $parser->getTable(),
                    'fields_down' => $this->getSchemaParser()->up(),
                    'fields_up'   => $this->getSchemaParser()->down(),
                ];
                break;
            default:
                $file = 'create';
                $replacements = [
                    'class'  => $this->getClass(),
                    'table'  => $parser->getTable(),
                    'fields' => $this->getSchemaParser()->up(),
                ];
                break;
        }
        $path = config('repository.generator.stubsOverridePath', __DIR__);

        if (!file_exists($path . "/Stubs/migration/{$file}.stub")) {
            $path = __DIR__;
        }

        if (!file_exists($path . "/Stubs/migration/{$file}.stub")) {
            throw new FileNotFoundException($path . "/Stubs/migration/{$file}.stub");
        }

        return Stub::create($path . "/Stubs/migration/{$file}.stub", $replacements);
    }


}