<?php
namespace Sinta\LRepository\Generators\Migrations;

use Illuminate\Contracts\Support\Arrayable;

class RulesParser implements Arrayable
{
    protected $rules;

    public function __construct($rules = null)
    {
        $this->rules = $rules;
    }


    public function toArray()
    {
        return $this->parse($this->rules);
    }


    public function parse($rules)
    {
        $this->rules = $rules;
        $parsed = [];
        foreach ($this->getRules() as $rulesArray) {
            $column = $this->getColumn($rulesArray);
            $attributes = $this->getAttributes($column, $rulesArray);
            $parsed[$column] = $attributes;
        }

        return $parsed;
    }

    public function getRules()
    {
        if (is_null($this->rules)) {
            return [];
        }

        return explode(',', str_replace(' ', '', $this->rules));
    }

    public function getColumn($rules)
    {
        return array_first(explode('=>', $rules), function ($key, $value) {
            return $value;
        });
    }

    public function getAttributes($column, $rules)
    {

        return str_replace($column . '=>', '', $rules);
    }

}