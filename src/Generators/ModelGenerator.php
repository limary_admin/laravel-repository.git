<?php
namespace Sinta\LRepository\Generators;

use Sinta\LRepository\Generators\Migrations\SchemaParser;

class ModelGenerator extends Generator
{
    protected $stub = 'model';

    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    public function getPathConfigNode()
    {
        return 'models';
    }

    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . '.php';
    }

    public function getBasePath()
    {
        return config('repository.generator.basePath', app()->path());
    }

    public function getReplacements()
    {
        return array_merge(parent::getReplacements(), [
            'fillable' => $this->getFillable()
        ]);
    }

    public function getFillable()
    {
        if (!$this->fillable) {
            return '[]';
        }
        $results = '[' . PHP_EOL;

        foreach ($this->getSchemaParser()->toArray() as $column => $value) {
            $results .= "\t\t'{$column}'," . PHP_EOL;
        }

        return $results . "\t" . ']';
    }

    public function getSchemaParser()
    {
        return new SchemaParser($this->fillable);
    }

}