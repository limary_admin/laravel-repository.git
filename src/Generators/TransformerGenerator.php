<?php
namespace Sinta\LRepository\Generators;


class TransformerGenerator extends Generator
{
    protected $stub = 'transformer/transformer';

    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    public function getPathConfigNode()
    {
        return 'transformers';
    }

    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . 'Transformer.php';
    }

    public function getBasePath()
    {
        return config('repository.generator.basePath', app()->path());
    }

    public function getReplacements()
    {
        $modelGenerator = new ModelGenerator([
            'name' => $this->name
        ]);
        $model = $modelGenerator->getRootNamespace() . '\\' . $modelGenerator->getName();
        $model = str_replace([
            "\\",
            '/'
        ], '\\', $model);

        return array_merge(parent::getReplacements(), [
            'model' => $model
        ]);
    }
}