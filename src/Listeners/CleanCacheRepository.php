<?php
namespace Sinta\LRepository\Listeners;

use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

use Sinta\LRepository\Contracts\RepositoryInterface;
use Sinta\LRepository\Events\RepositoryEventBase;
use Sinta\LRepository\Helpers\CacheKeys;

/**
 * 清除缓存仓库
 *
 * Class CleanCacheRepository
 * @package Sinta\LRepository\Listeners
 */
class CleanCacheRepository
{
    //CacheRepository
    protected $cache = null;

    //RepositoryInterface
    protected $repository = null;

    //@var Model
    protected $model = null;

    //@var string
    protected $action = null;


    public function __construct()
    {
        $this->cache = app(config('repository.cache.repository'),'cache');
    }


    /**
     * 处理器
     *
     * @param RepositoryEventBase $event
     */
    public function handle(RepositoryEventBase $event)
    {
        try{
            $cleanEnabled = config("repository.cache.clean.enabled", true);

            if($cleanEnabled){
                $this->repository = $event->getRepository();
                $this->model = $event->getModel();
                $this->action = $event->getAction();

                if (config("repository.cache.clean.on.{$this->action}", true)) {
                    $cacheKeys = CacheKeys::getKeys(get_class($this->repository));
                    if (is_array($cacheKeys)) {
                        foreach ($cacheKeys as $key) {
                            $this->cache->forget($key);
                        }
                    }
                }
            }

        }catch(\Exception $e){
            Log::error($e->getMessage());
        }
    }

}