<?php
namespace Sinta\LRepository\Contracts;

/**
 *
 *
 * Interface Presentable
 * @package Sinta\LRepository\Contracts
 */
interface Presentable
{
    public function setPresenter(PresenterInterface $presenter);

    public function presenter();
}