<?php
namespace Sinta\LRepository\Contracts;


interface PresenterInterface
{
    /**
     * 准备数据
     *
     * @param $data
     * @return mixed
     */
    public function present($data);
}