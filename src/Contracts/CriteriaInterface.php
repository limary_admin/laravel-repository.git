<?php
namespace Sinta\LRepository\Contracts;


interface CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository);
}