<?php
namespace Sinta\LRepository\Events;


class RepositoryEntityDeleted extends RepositoryEventBase
{
    protected $action = 'deleted';
}