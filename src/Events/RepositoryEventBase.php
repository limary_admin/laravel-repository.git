<?php
namespace Sinta\LRepository\Events;


use Illuminate\Database\Eloquent\Model;
use Sinta\LRepository\Contracts\RepositoryInterface;


abstract class RepositoryEventBase
{

    protected $model;

    protected $repository;

    protected $action;

    public function __construct( RepositoryInterface $repository, Model $model)
    {
        $this->repository = $repository;
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }


    public function getRepository()
    {
        return $this->repository;
    }


    public function getAction()
    {
        return $this->action;
    }
}